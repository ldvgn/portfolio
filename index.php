<?php

    require_once "controllers/frontend/home.controller.php";
    require_once "controllers/frontend/projects.controller.php";
    require_once "controllers/frontend/project.controller.php";
    require_once "controllers/frontend/skills.controller.php";
    require_once "controllers/frontend/contact.controller.php";

    $homeController      = new HomeController();
    $projectsController  = new ProjectsController();
    $projectController   = new ProjectController();
    $skillsController    = new SkillsController();
    $contactController   = new ContactController();

    try{
        if(empty($_GET['page'])){
            header('Location: '.URL."accueil");
        } else {
            $url = explode("/", filter_var($_GET["page"], FILTER_SANITIZE_URL));
            switch($url[0]){
                case "accueil" :     $homeController -> getHomepage();
                break;
                case "projets" :     $projectsController -> getProjectsPage();
                break;
                case "projet":       $projectController -> getProjectPage();
                break;
                case "profil" :      $skillsController  -> getSkillsPage();
                break;
                case "contact" :     $contactController -> getContactPage(); 
                break;
                case "sendContact" : $contactController -> sendContact(); 
                break;
                case "cgu" :         $homeController    -> getCguPage(); 
                break;
                default: throw new Exception("La page n'existe pas");
            }
        }
    }
    catch(Exception $e){
        $errorMessage = $e->getMessage();
        echo($errorMessage);
    }