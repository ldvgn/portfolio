const cacheName = 'cache-v1';
const precacheResources = [
    '/',
    'index.php',
    'accueil',
    'projets',
    'projets/projet',
    'profil',
    'contact',
    'cgu',
    'public/js/app.js',
    'public/js/script.js',
    'public/styles/style.min.css',
    'sources/files',
    'sources/images',
    'sources/images/pictos',
    'sources/images/projects',
    'sources/images/resume',
    'sources/images/skills'
];

self.addEventListener('install', event => {
    // console.log('Service worker install event!');
    event.waitUntil(
        // Open a cache of resources.
        caches.open(cacheName).then(cache => {
            // Begins the process of fetching them.
            // The coast is only clear when all the resources are ready.
            return cache.addAll(precacheResources);
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request).then(cachedResponse => {
            if (cachedResponse) {
                return cachedResponse;
            }
            return fetch(event.request);
        }));
});


// self.addEventListener('activate', event => {
//     console.log('Service worker activate event!');
// });
// self.addEventListener("activate", function(e) { 
//     self.registration.unregister() .then(function() { 
//         return self.clients.matchAll(); 
//     }) 
//     .then(function(clients) { 
//         clients.forEach(client => client.navigate(client.url)); 
//     }); 
// }); 