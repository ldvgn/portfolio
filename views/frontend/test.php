        
<div class="projects">
    <div class="projects-content">
        <?php if(empty($projects)) : ?>
            <div class="projects-alert">
                <div class="alert alert-danger text-center" role="alert">
                    Oops ! Il n'y a pas de projets dans cette catégorie =(
                </div>
            </div>
        <?php else: ?>
            <?php foreach($projects as $project) : ?>
                <div class="projects-list">
                    <div class="projects-list-item">
                        <img src="<?=URL?>public/sources/images/<?= $project['image']?>" alt="<?= $project['alt']?>" class="img-fluid">
                        <div class="projects-list-item--overlay">
                            <form method="POST" action="<?= URL ?>projets/projet&idProjet=<?= $project['id']?>">
                                <button type="submit">VOIR LE PROJET</button>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php endif ?>

    </div>
</div>