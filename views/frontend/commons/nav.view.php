<!-- mobile navigation -->
<div class="d-md-none">
    <nav class="nav__mobile">
        <ul>
            <li>
                <a href="<?= URL ?>accueil">
                    <svg class="icon <?php if($_GET['page'] == "accueil") {echo 'active';} ?>">
                        <use xlink:href="<?= URL ?>public/sources/images/sprite.svg#icon-home"></use>
                    </svg>
                    <p class="<?php if($_GET['page'] == "accueil") {echo 'active';} ?>">Accueil</p>
                </a>
            </li>
            <li>
                <a href="<?= URL ?>projets">
                    <svg class="icon <?php if($_GET['page'] == "projets" || $_GET['page'] == "projets/all" || $_GET['page'] == "projets/maquettage" || $_GET['page'] == "projets/website" || $_GET['page'] == "projets/dao") {echo 'active';} ?>">
                        <use xlink:href="<?= URL ?>public/sources/images/sprite.svg#icon-project"></use>
                    </svg>
                    <p class="<?php if($_GET['page'] == "projets" || $_GET['page'] == "projets/all" || $_GET['page'] == "projets/maquettage" || $_GET['page'] == "projets/website" || $_GET['page'] == "projets/dao") {echo 'active';} ?>">Projets</p>
                </a>
            </li>
            <li>
                <a href="<?= URL ?>profil">
                    <svg class="icon <?php if($_GET['page'] == "profil") {echo 'active';} ?>">
                        <use xlink:href="<?= URL ?>public/sources/images/sprite.svg#icon-profil"></use>
                    </svg>
                    <p class="<?php if($_GET['page'] == "profil") {echo 'active';} ?>">Profil</p>
                </a>
            </li>
            <li>
                <a href="<?= URL ?>contact">
                    <svg class="icon <?php if($_GET['page'] == "contact") {echo 'active';} ?>">
                        <use xlink:href="<?= URL ?>public/sources/images/sprite.svg#icon-contact"></use>
                    </svg>
                    <p class="<?php if($_GET['page'] == "contact") {echo 'active';} ?>">Contact</p>
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="d-none d-md-block">
    <nav class="nav">
        <div class="nav__brand">
            <a href="<?= URL ?>accueil">
                <img src="<?= URL ?>public/sources/images/LauryDevraigne_logo.svg" alt="Le portfolio de Laury Devraigne" class="my-1">
            </a>
        </div>
        <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav__content">
                <li class="nav__item">
                    <a class="nav__link <?php if($_GET['page'] == "accueil") {echo 'active';} ?>" href="<?= URL ?>accueil">Accueil</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link <?php if($_GET['page'] == "projets") {echo 'active';} ?>" href="<?= URL ?>projets">Projets</a>
                </li>
                <li class="nav__item">
                    <a class="nav__link <?php if($_GET['page'] == "profil") {echo 'active';} ?>" href="<?= URL ?>profil">Profil</a>
                </li>
                <form method="get" action="<?= URL ?>contact" class="nav__item">
                    <button type="submit">Contactez-moi</button>
                </form>
            </ul>
        </div>
    </nav>
</div>
