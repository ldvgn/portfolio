<div class="banner">
    <div class="row">
        <div class="banner-content">
            <h2>Travaillez ensemble ?</h2>
            <h3>Je suis à la recherche d'un emploi</h3>
            <a href="<?= URL ?>contact" class="button--primary">Contactez-moi</a>
        </div>
    </div>
</div>