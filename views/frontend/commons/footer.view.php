<footer>
    <div class="content">
        <div class="content-list">
            <ul>
                <li>
                    <a href="<?= URL ?>cgu">CGU</a>
                </li>
                <li>
                    <a href="sitemap.xml" target="_blank">Sitemap</a>
                </li>
            </ul>
        </div>
        <div class="content-list">
            <ul>
                <li>
                    <a href="<?= URL ?>projets">Projets</a>
                </li>
                <li>
                    <a href="<?= URL ?>profil">Profil</a>
                </li>
                <li>
                    <a href="<?= URL ?>contact">Contact</a>
                </li>
            </ul>
        </div>
        <div class="content-rs">
            <a href="<?= $datas['linkedin']?>" target="_blank">
                <img src="<?=URL?>public/sources/images/pictos/in--secondary.svg" alt="" class="img-fluid" onmouseover=socialmediaLinkedinHover(this); onmouseout=socialmediaLinkedin(this);>
            </a>
            <a href="<?= $datas['instagram']?>" target="_blank">
                <img src="<?=URL?>public/sources/images/pictos/insta--secondary.svg" alt="" class="img-fluid" onmouseover=socialmediaInstaHover(this); onmouseout=socialmediaInsta(this);>
            </a>
        </div>
    </div>
</footer>