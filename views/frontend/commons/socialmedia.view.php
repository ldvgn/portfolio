<div class="socialmedia d-none d-md-block link--shadow">
    <ul class="m-0">
        <li>
            <a href="<?= $datas['linkedin']?>" target="_blank">
                <img src="<?=URL?>public/sources/images/pictos/in.svg" alt="" class="my-1">
                <div class="slider">
                    <p>LinkedIn</p>
                </div>
            </a>
        </li>
        <li>
            <a href="<?= $datas['instagram']?>" target="_blank">
                <img src="<?=URL?>public/sources/images/pictos/insta.svg" alt="" class="my-1">
                <div class="slider">
                    <p>Instagram</p>
                </div>
            </a>
        </li>
    </ul>
</div>