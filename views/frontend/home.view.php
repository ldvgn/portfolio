<?php ob_start();?>


<?php require "views/frontend/commons/nav.view.php"; ?>

<div class="home__intro">
    <div class="home__intro-content">
        <div class="home__intro-item">
            <h1><?= $datasPerson['name'].' '.$datasPerson['surname']?></h1>
            <h2><?= $datasPerson['job']?></h2>
            <p><?= $datasPerson['description']?></p>
            <a href="<?= $datasPerson['cv']?>" target="_blank">Télécharger le CV</a>
        </div>
        <div class="home__intro-item">
            <img src="<?= $datasPerson['image']?>" alt="<?= $datasPerson['alt']?>" class="img-fluid">
        </div>
    </div>
</div>

<div class="home__skill">
    <h2>Ce que je peux faire ?</h2>
    <div class="home__skill-content">
        <?php foreach($resumeItems as $item): ?>
            <div class="home__skill-item">
                <img src="<?=URL?>public/sources/images/<?= $item['image']?>" alt="<?= $item['alt']?>" class="img-fluid">
                <h3><?= $item['title']?></h3>
                <p><?= $item['description']?></p>
            </div>
        <?php endforeach?>
    </div>
</div>

<div class="home__news">
    <div class="row">
        <div class="col-12">
            <h2>Mes derniers projets</h2>
        </div>
        <div class="container">
            <div class="home__news-content">
                <?php foreach($projects as $project) : ?>
                    <div class="home__news-item">
                        <img src="<?=URL?>public/sources/images/<?= $project['image']?>" alt="<?= $project['alt']?>" class="img-fluid">
                        <div class="home__news-item--overlay">
                            <form method="POST" action="<?= URL ?>projets/projet&idProjet=<?= $project['id']?>">
                                <button type="submit">VOIR LE PROJET</button>
                            </form>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>

<?php 
    require "views/frontend/commons/banner.view.php";
    require "views/frontend/commons/footer.view.php";

    $content = ob_get_clean();
    $title = "Laury | Accueil";
    $description = "Bienvenue sur le portfolio de Laury Devraigne, webdesigner et développeuse web junior.";
    require "views/frontend/template/tml_front.php";