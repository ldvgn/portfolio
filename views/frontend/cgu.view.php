<?php ob_start();?>

<?php require "views/frontend/commons/nav.view.php"; ?>

<section class="cgu">
    <h1 class="cgu-title">Conditions générales d'utilisation</h1>
    <p class="cgu-date">En vigueur au 10/03/2021</p>
    <p>Merci de lire avec attention les différentes modalités d’utilisation du présent site avant d’y parcourir ses pages. 
       En vous connectant sur ce site, vous acceptez, sans réserves, les présentes modalités.
       Aussi, conformément à l’article n°6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance dans l’économie numérique, 
       les responsables du présent site internet www.laurydevraigne.fr sont :</p>
     
    <h2 class="cgu-title--underline">Éditeur du Site:</h2>
    <p>
        Laury DEVRAIGNE<br/>
        Site Web: www.laurydevraigne.fr
    </p>

    <h2 class="cgu-title--underline">Hébergement:</h2>
    <p>
        Hébergeur: INFOMANIAK<br/>
        Rue Eugène-Marziano 25, 1227 Genève, Suisse<br/>
        Site Web: www.infomaniak.com
    </p>
    

    <h2 class="cgu-title--color">I- Accès au site:</h2>
    <p>Le site www.laurydevraigne.fr propose: présentation d'un portfolio personnel.</p>
    <p>Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les
        frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion
        Internet, etc.) sont à sa charge.</p>

    <h2 class="cgu-title--color">II- Collecte des données</h2>
    <p>Le site est exempté de déclaration à la Commission Nationale Informatique et Libertés (CNIL) dans la
    mesure où il ne collecte aucune donnée concernant les Utilisateurs.</p>

    <h2 class="cgu-title--color">III- Propriété intellectuelle</h2>
    <p>Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l'objet
        d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.
        L'Utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des
        différents contenus. Il s'engage à une utilisation des contenus du site dans un cadre strictement privé,
        toute utilisation à des fins commerciales et publicitaires est strictement interdite.
        Toute représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l’autorisation
        expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2
        et suivants du Code de la propriété intellectuelle.
        Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui
        reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.</p>

    <h2 class="cgu-title--color">IV- Responsabilité</h2>
    <p>Les sources des informations diffusées sur le site www.laurydevraigne.fr sont réputées fiables mais le
        site ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
        Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle.
        Malgré des mises à jour régulières, le site www.laurydevraigne.fr ne peut être tenu responsable de la
        modification des dispositions administratives et juridiques survenant après la publication. De même, le
        site ne peut être tenue responsable de l’utilisation et de l’interprétation de l’information contenue dans
        ce site.
        Le site www.laurydevraigne.fr ne peut être tenu pour responsable d’éventuels virus qui pourraient
        infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou
        au téléchargement provenant de ce site.
        La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et
        insurmontable d'un tiers.
        </p>

    <h2 class="cgu-title--color">V- Cookies</h2>
    <p>L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement
        sur son logiciel de navigation.
        Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de
        l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site www.laurydevraigne.fr
        Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier
        quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains
        cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.
        L’information contenue dans les cookies est utilisée pour améliorer le site www.laurydevraigne.fr
        En naviguant sur le site, L’Utilisateur les accepte.
        L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son
        logiciel de navigation.
        </p>
</section>

<?php 
    require "views/frontend/commons/footer.view.php";

    $content = ob_get_clean();
    $title = "Laury | CGU";
    $description = "";
    require "views/frontend/template/tml_front.php";