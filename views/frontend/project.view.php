<?php 
    ob_start(); 
    require "views/frontend/commons/nav.view.php";
?>

<div class="project">
    <div class="project-content">
        <div class="project-info">
            <h1><?= $datasProject['title']?></h1>
            <p><?= $datasProject['description']?></p>
            <div class="project-info--link">
                <ul>
                    <?php if(!empty($datasProject['code'])): ?>
                        <li>
                            <a href="<?= $datasProject['code']?>" target="_blank">VOIR LE CODE</a>
                        </li>
                    <?php endif ?>
                    <?php if(!empty($datasProject['website'])): ?>
                        <li>
                            <a href="<?= $datasProject['website']?>" target="_blank">VOIR LE SITE</a>
                        </li>
                    <?php endif ?>
                    <?php if(!empty($datasProject['pdf'])): ?>
                        <li>
                            <a href="<?= URL ?>public/sources/files/<?= $datasProject['pdf']?>" target="_blank">VOIR LE PDF</a>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
            <div class="project-info--tag">
                <p>Tag(s):</p>
                <div class="project-info--tag-item">
                    <?php foreach($tags as $tag): ?>
                        <?php if ($tag['tag'] === 'maquettage'): ?>
                            <a href="<?= URL ?>projets/maquettage">Maquettage</a>
                        <?php elseif ($tag['tag'] === 'website'): ?>
                            <a href="<?= URL ?>projets/website">Website</a>
                        <?php else: ?>
                            <a href="<?= URL ?>projets/dao">DAO</a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="project-gallery">
            <div class="container">
                <div class="row image-gallery mx-0">
                    <?php foreach($images as $k => $image): ?>
                        <div class="col-6 p-0">
                            <a href="<?= URL ?>public/sources/images/<?= $image['url'] ?>" data-toggle="modal" data-target="#exampleModal<?= $k ?>">
                                <img src="<?= URL ?>public/sources/images/<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="image-gallery__image image-gallery__col"/>
                            </a>

                            <div class="modal fade" id="exampleModal<?= $k ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?= $k ?>" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title" id="exampleModalLabel<?= $k ?>"><?= $image['alt'] ?></h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?= URL ?>public/sources/images/<?= $image['url'] ?>" alt="<?= $image['alt'] ?>" class="img-fluid"/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
    require "views/frontend/commons/footer.view.php";
    
    $content = ob_get_clean();
    $title = "Laury | Projets";
    $description = "Des projets web et DAO pour montrer quelques unes de mes compétences";
    require "views/frontend/template/tml_front.php";