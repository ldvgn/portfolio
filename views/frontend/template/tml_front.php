<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name= "description" content= "<?= $description ?>" />
    <link rel="icon" href="favicon.ico" />

    <!-- CSS -->
    <link href="<?= URL ?>public/styles/style.min.css" rel="stylesheet" type="text/css">

    <!-- <link href="<?= URL ?>public/styles/prefixed/style.min.css" rel="stylesheet" type="text/css"> -->

    <!-- PWA -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= URL ?>public/sources/pwa/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= URL ?>public/sources/pwa/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= URL ?>public/sources/pwa/favicon-16x16.png">
    <link rel="manifest" href="<?= URL ?>manifest.json">
    <link rel="mask-icon" href="<?= URL ?>public/sources/pwa/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    
    <title><?= $title ?></title>
    
</head>
<body>
    <section id="installBanner" class="install-banner">
        <button id="closeBtn" class="install-banner--close">&#10006;</button>
        <p>Ajouter mon portfolio à l'écran d'accueil</p>
        <button id="installBtn" class="install-banner--install">Installer</button>
    </section>

    <div id="content">
        <?= $content ?>
    </div>
    
    <script src="<?= URL ?>vendors/jquery/jquery-3.5.1.slim.min.js"></script>
    <script src="<?= URL ?>vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= URL ?>public/js/script.js"></script>
    <script src="<?= URL ?>public/js/app.js"></script>
</body>
</html>