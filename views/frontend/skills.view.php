<?php 
    ob_start(); 
    require "views/frontend/commons/nav.view.php";
?>

<div class="skills">
    <div class="skills-content">
        <div class="skills-title">
            <h1>Mes compétences</h1>
        </div>
        <?php foreach($skills as $skill) : ?>
            <div class="skills-item">
                <img src="<?=URL?>public/sources/images/<?= $skill['image']?>" alt="<?= $skill['alt']?>" class="img-fluid">
                <h2><?= $skill['title']?></h2>
                <p><?= $skill['description']?></p>
            </div>
        <?php endforeach ?>
    </div>
</div>

<?php 
    require "views/frontend/commons/footer.view.php";

    $content = ob_get_clean();
    $title = "Laury | Compétences";
    $description = "Mes compétences en langages de programmation et logiciels";
    require "views/frontend/template/tml_front.php";