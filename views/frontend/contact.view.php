<?php 
    ob_start(); 
    session_start();
    require "views/frontend/commons/nav.view.php";
?>
<div class="contact">
    <div class="contact-alert">
        <?php if(!empty($_SESSION['errors'])): ?>
            <div class="alert alert-danger">
                <?= implode('<br/>', $_SESSION['errors']); ?>
            </div>
        <?php  endif; ?>
        <?php if(!empty($_SESSION['success'])): ?>
            <div class="alert alert-success">
                Votre message m'est parvenu.
            </div>
        <?php  endif; ?>
    </div>
    
    <form action="<?= URL ?>sendContact" method="POST">
        <div class="contact-content">
            <div class="contact-title">
                <h1>Formulaire de contact</h1>
            </div>
            <div class="contact-input--small">
                <div class="form-group ">
                    <label for="inputname">Votre nom</label>
                    <input type="text" name="name" class="form-control" id="inputname" value="<?= isset($_SESSION['inputs']['name']) ? $_SESSION['inputs']['name'] : ''; ?>" required>
                </div>
            </div>
            <div class="contact-input--small">
                <div class="form-group ">
                    <label for="inputemail">Votre email</label>
                    <input type="text" name="email" class="form-control" id="inputemail" value="<?= isset($_SESSION['inputs']['email']) ? $_SESSION['inputs']['email'] : ''; ?>" required>
                </div>
            </div>
            <div class="contact-input--large">
                <div class="form-group">
                    <label for="inputmessage">Votre message</label>
                    <textarea class="form-control" name="message" id="inputmessage" rows="10" required><?= isset($_SESSION['inputs']['message']) ? $_SESSION['inputs']['message'] : ''; ?></textarea>
                </div>
            </div>
            <div class="contact-button">
                <button class="button--primary" type="submit">ENVOYER</button>
            </div>
        </div>
    </form>
</div>


<?php
    require "views/frontend/commons/footer.view.php";
    unset($_SESSION['inputs']);
    unset($_SESSION['errors']);
    unset($_SESSION['success']);

    $content = ob_get_clean();
    $title = "Laury | Contact";
    $description = "Si vous souhaitez joindre Laury Devraigne, vous pouvez la contacter via cette page formulaire.";
    require "views/frontend/template/tml_front.php";