<?php 
    ob_start();
    require "views/frontend/commons/nav.view.php";
?>

<div class="projects">
    <div class="projects-content">
        <div class="projects-item">
            <h1>Mon portfolio</h1>

            <div class="projects-item--tag">
                <button class="projects-item--tag-link" name="all">Tous</button>
                <?php foreach($tags as $tag) : ?>
                    <button class="projects-item--tag-link" name="<?= $tag['name'] ?>"><?= $tag['name'] ?></button>
                <?php endforeach ?>
            </div>
        </div>
        
        <div class="projects-alert" id="projects-alert">
            <div class="alert alert-danger text-center" role="alert">
                Oops ! Il n'y a pas de projets dans cette catégorie =(
            </div>
        </div>

        <?php if(empty($projects)) : ?>
            <div class="projects-alert">
                <div class="alert alert-danger text-center" role="alert">
                    Oops ! Il n'y a plus de projets =(
                </div>
            </div>
        <?php else: ?>
            <?php foreach($projects as $project) : ?>
                <div class="projects-list <?= $project['tag'] ?>">
                    <div class="projects-list-item">
                        <img src="<?=URL?>public/sources/images/<?= $project['image']?>" alt="<?= $project['alt']?>" class="img-fluid">
                        <div class="projects-list-item--overlay">
                            <form method="POST" action="<?= URL ?>projet&idProjet=<?= $project['id']?>">
                                <button type="submit">VOIR LE PROJET</button>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</div>

<?php 
    require "views/frontend/commons/footer.view.php";

    $content = ob_get_clean();
    $title = "Laury | Projets";
    $description = "Des projets web et DAO pour montrer quelques unes de mes compétences";
    require "views/frontend/template/tml_front.php";