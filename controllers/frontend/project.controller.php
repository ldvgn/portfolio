<?php
require_once "config/config.php";
require_once "models/frontend/project.model.php";
require_once "controllers/frontend/socialmedia.controller.php";

/**
 * Class ProjectController | file project.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - get projects page with DB informations
 * - treats datas for readability into the view
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./models/project.model.php";
 * - require "./controllers/socialmedia.controller.php";
 *
 * @package Portfolio
 * @subpackage Project Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class ProjectController{
    /**
     * private attribute projectModel 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $projectModel;
    
    /**
     * private attribute socialmediaController 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaController;

    /**
     * Get instance of projectModel
     * Get instance of socialmediaController
     */ 
    public function __construct(){
        $this->projectModel = new ProjectModel();
        $this->socialmediaController = new SocialmediaController();
    }


    /**
     * Method getProjectPage()
     * 
     * Collect several informations about the project selected and get the page about this project selected with these informations thanks to the DB
     */
    public function getProjectPage(){
        if(isset($_GET['idProjet']) && !empty($_GET['idProjet'])){
            $idProject = Security::secureHTML($_GET['idProjet']);
    
            $project      = $this->projectModel -> getProjectInfos($idProject);
            $images       = $this->projectModel -> getProjectImages($idProject);
            $imageActive  = $this->projectModel -> getProjectImagesActive($idProject);
            $tags         = $this->projectModel -> getProjectTag($idProject);

            $datasProject = $this->treatdatasForProject($project);
            $datasImagesActive = $this->treatdatasForImageActive($imageActive);

            $datas  = $this->socialmediaController -> getSocialmedia();
            require_once "views/frontend/project.view.php";
        }
        else{
            throw new Exception("Vous ne pouvez pas accéder à la page");
        }
    }

    /**
     * Method treatdatasForProject($lignes)
     * 
     * Change array's datas name for more readability and security
     * 
     * @return array
     */
    private function treatdatasForProject($lignes){
        $tab = [];
        foreach ($lignes as $ligne){
            $tab = [
                "title"       => $ligne['project_title'],
                "description" => $ligne['project_description'],
                "website"     => $ligne['project_website'],
                "pdf"         => $ligne['project_pdf'],
                "code"        => $ligne['project_code']
            ];
        }
        return $tab;
    }

    /**
     * Method treatdatasForImageActive($lignes)
     * 
     * Change array's datas name for more readability and security
     * 
     * @return array
     */
    private function treatdatasForImageActive($lignes){
        $tab = [];
        foreach ($lignes as $ligne){
            $tab = [
                "url" => $ligne['image_url'],
                "alt" => $ligne['image_alt']
            ];
        }
        return $tab;
    }

}