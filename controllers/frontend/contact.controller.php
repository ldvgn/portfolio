<?php
require_once "config/config.php";
require_once "controllers/frontend/socialmedia.controller.php";

/**
 * Class ContactController | file contact.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - get contact page
 * - verify if input fields required aren't empty and send email if it's right
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./controllers/socialmedia.controller.php";
 *
 * @package Portfolio
 * @subpackage Contact Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class ContactController{
    /**
     * private attribute socialmediaController 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaController;

    /**
     * Get instance of socialmediaController
     */ 
    public function __construct(){
        $this->socialmediaController = new SocialmediaController();
    }

    /**
     * Method getContactPage()
     * 
     * Collect DB informations about socialMedia link and get the contact page
     */
    public function getContactPage(){
        $datas = $this->socialmediaController -> getSocialmedia();
        require_once "views/frontend/contact.view.php";
    } 


    /**
     * Method sendContact()
     * 
     * Send email if all required input fields aren't empty
     */
    public function sendContact(){
        $errors = [];

        /* Contrôle de saisie */
        if(!isset($_POST['name']) || empty($_POST['name'])){
            $errors['name'] = "Vous n'avez pas renseigné votre nom";
        }
        if(!isset($_POST['email']) || empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] = "Vous n'avez pas renseigné un email valide";
        }
        if(!isset($_POST['message']) || empty($_POST['message'])){
            $errors['message'] = "Vous n'avez pas renseigné votre message";
        }

        /* envoi du mail ou non en fonction de la variable de session */
        session_start();
        if(!empty($errors)){
            $_SESSION['errors'] = $errors;
            $_SESSION['inputs'] = $_POST;
            header('Location: '.URL."contact/");
        }
        else{
            $_SESSION['success'] = 1;
            $message = $_POST['message'];
            $name = $_POST['name'];
            $headers = 'FROM ' . $_POST['email'];
            mail('l.devraigne@gmail.com', 'Demande de contact de ' . $name, $message, $headers);
            header('Location: '.URL."contact/");
        }
    }
}