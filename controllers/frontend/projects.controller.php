<?php
require_once "config/config.php";
require_once "config/Security.class.php";
require_once "models/frontend/projects.model.php";
require_once "controllers/frontend/socialmedia.controller.php";

/**
 * Class ProjectsController | file projects.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - filter projects thanks to tag
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./models/projects.model.php";
 * - require "./config/Security.class.php";
 * - require "./controllers/socialmedia.controller.php";
 *
 * @package Portfolio
 * @subpackage Projects Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class ProjectsController{
    /**
     * private attribute projectsModel 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $projectsModel;
    
    /**
     * private attribute socialmediaController 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaController;

    /**
     * Get instance of projectsModel
     * Get instance of socialmediaController
     */
    public function __construct(){
        $this->projectsModel = new ProjectsModel();
        $this->socialmediaController = new SocialmediaController();
    }

    /**
     * Method getProjectsPage()
     * 
     * Method to filter projects thanks to tag
     */

    public function getProjectsPage(){

        $projects = $this->projectsModel -> getProjects();
        $tags     = $this->projectsModel -> getTags();

        $datas      = $this->socialmediaController -> getSocialmedia();
        require_once "views/frontend/projects.view.php";
    }
}