<?php
require_once "config/config.php";
require_once "models/frontend/home.model.php";
require_once "controllers/frontend/socialmedia.controller.php";

/**
 * Class HomeController | file home.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - get homepage with DB informations
 * - treats datas for readability into the view
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./models/home.model.php";
 * - require "./controllers/socialmedia.controller.php";
 *
 * @package Portfolio
 * @subpackage Home Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class HomeController{
     /**
     * private attribute homeModel 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $homeModel;
    /**
     * private attribute socialmediaController 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaController;

    /**
     * Get instance of socialmediaController
     * Get instance of homeModel
     */ 
    public function __construct(){
        $this->homeModel = new HomeModel();
        $this->socialmediaController = new SocialmediaController();
    }

    /**
     * Method getHomepage()
     * 
     * Collect several informations about the first person (cf Laury) and get the homepage view
     */
    public function getHomepage(){
        $idPerson = 1;
        $person      = $this->homeModel -> getPerson($idPerson);
        $resumeItems = $this->homeModel -> getResume();
        $projects    = $this->homeModel -> getNewsProjects();
        $datas       = $this->socialmediaController -> getSocialmedia();
        
        $datasPerson = $this->treatdatasForPerson($person);
        require_once "views/frontend/home.view.php";
    }

    /**
     * Method treatdatasForPerson($lignes)
     * 
     * Change array's datas name for more readability and security
     * 
     * @return array
     */
    private function treatdatasForPerson($lignes){
        $tab = [];
        foreach ($lignes as $ligne){
            $tab = [
                "name"        => $ligne['person_name'],
                "surname"     => $ligne['person_surname'],
                "job"         => $ligne['person_job'],
                "image"       => URL."public/sources/images/".$ligne['person_image'],
                "alt"         => $ligne['person_alt'],
                "description" => $ligne['person_description'],
                "cv"          => URL."public/sources/files/".$ligne['person_cv']
            ];
        }
        return $tab;
    }

    /**
     * Method getHomepage()
     * 
     * Collect several informations about the first person (cf Laury) and get the homepage view
     */
    public function getCguPage(){
        $datas       = $this->socialmediaController -> getSocialmedia();
        require_once "views/frontend/cgu.view.php";
    }
}