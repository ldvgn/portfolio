<?php
require_once "config/config.php";
require_once "models/frontend/socialmedia.model.php";

/**
 * Class SocialmediaController | file socialmedia.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - create the socialmedia bloc
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./models/socialmedia.model.php";
 *
 * @package Portfolio
 * @subpackage SocialMedia Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class SocialmediaController{
    /**
     * private attribute socialmediaModel 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaModel;

    /**
     * Get instance of socialmediaModel
     */ 
    public function __construct(){
        $this->socialmediaModel = new SocialmediaModel();
    }

    /**
     * Method getSocialmedia()
     * 
     * Collect DB's informations about socialmedia
     * 
     * @return array
     */
    public function getSocialmedia(){
        $idPerson = 1;
        $socialmedia = $this->socialmediaModel -> getSocialmedia($idPerson);

        $datas = $this->treatdatasForSocialMedia($socialmedia);
        return $datas;
    }

    /**
     * Method treatdatasForSocialMedia($lignes)
     * 
     * Change array's datas name for more readability and security
     * 
     * @return array
     */
    private function treatdatasForSocialMedia($lignes){
        $tab = [];
        foreach ($lignes as $ligne){
            $tab = [
                "linkedin"  => $ligne['person_in'],
                "instagram" => $ligne['person_insta']
            ];
        }
        return $tab;
    }
}