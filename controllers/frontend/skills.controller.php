<?php
require_once "config/config.php";
require_once "models/frontend/skills.model.php";
require_once "controllers/frontend/socialmedia.controller.php";

/**
 * Class SkillsController | file skills.controller.php
 * 
 * In this class, we have methods for :
 * 
 * - filter projects thanks to tag
 *
 * Is required:
 * 
 * - require "./config/config.php";
 * - require "./models/project.model.php";
 * - require "./config/Security.class.php";
 * - require "./controllers/socialmedia.controller.php";
 *
 * @package Portfolio
 * @subpackage Skills Controller
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class SkillsController{
    /**
     * private attribute skillsModel 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $skillsModel;

    /**
     * private attribute socialmediaController 
     * 
     * Is used to store all datas needed for views
     * 
     * @var object
     * 
     */
    private $socialmediaController;

    /**
     * Get instance of skillsModel
     * Get instance of socialmediaController
     */ 
    public function __construct(){
        $this->skillsModel = new SkillsModel();
        $this->socialmediaController = new SocialmediaController();
    }

    /**
     * Method getSkillsPage()
     * 
     * Collect DB's informations about skills and print then into view 
     */
    public function getSkillsPage(){
        $idPerson   = 1;
        $skills     = $this->skillsModel -> getSkills($idPerson);
        $datas      = $this->socialmediaController -> getSocialmedia();
        require_once "views/frontend/skills.view.php";
    }
}