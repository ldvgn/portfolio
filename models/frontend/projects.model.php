<?php
require_once "./models/initialize.php";

class ProjectsModel extends Initialize {

    public function getProjects(){
        $req = 'SELECT p.project_id as id, image_url as image, image_alt as alt, GROUP_CONCAT(tag_name SEPARATOR " ") as tag
                FROM project as p 
                INNER JOIN image ON image.project_id = p.project_id 
                INNER JOIN person__project as pp ON pp.project_id = p.project_id 
                INNER JOIN project__tag as pt ON pt.project_id = p.project_id 
                INNER JOIN tag as t ON t.tag_id = pt.tag_id 
                WHERE image_thumb = 0 AND pp.person_id = 1
                GROUP BY p.project_id
                ORDER BY p.project_id DESC';

        $stmt = $this->getDB()->prepare($req);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }

    public function getTags(){
        $req = 'SELECT tag_name as name
                FROM tag';
        $stmt = $this->getDB()->prepare($req);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }

    public function getProjectsWithTags($tag, $idPerson){
        $req = 'SELECT p.project_id as id, image_url as image, image_alt as alt, tag_name
                FROM project as p
                INNER JOIN image as i ON i.project_id = p.project_id
                INNER JOIN person__project as pp ON pp.project_id = p.project_id
                INNER JOIN project__tag as pt ON pt.project_id = p.project_id
                INNER JOIN tag as t ON t.tag_id = pt.tag_id
                WHERE image_thumb = 0 AND pp.person_id = :idPerson AND tag_name = :tag
                ORDER BY p.project_id DESC';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":tag", $tag, PDO::PARAM_STR);
        $stmt->bindValue(":idPerson", $idPerson, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }
}