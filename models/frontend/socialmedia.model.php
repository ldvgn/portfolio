<?php
require_once "./models/initialize.php";

class SocialmediaModel extends Initialize {
    public function getSocialmedia($idPerson){
        $req = 'SELECT person_in, person_insta FROM person
                WHERE person_id = :idPerson';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idPerson", $idPerson, PDO::PARAM_INT);
        $stmt->execute();
        $socialmedia = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $socialmedia;
    }
}