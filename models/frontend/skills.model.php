<?php
require_once "./models/initialize.php";

class SkillsModel extends Initialize {
    public function getSkills($idPerson){
        $req = 'SELECT skill_title as title, skill_description as description, skill_image as image, skill_alt as alt 
                FROM skill as s
                INNER JOIN person__skill as ps ON s.skill_id = ps.skill_id
                WHERE ps.person_id = :idPerson';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idPerson", $idPerson, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }
}