<?php
require_once "./models/initialize.php";

/**
 * Class HomeModel | file home.model.php
 * 
 * In this class, we have methods for :
 * 
 * - Select DB's informations about a person (cf Laury) and these projects
 *
 * Is required:
 * 
 * - require "./models/initialize.php";
 *
 * @package Portfolio
 * @subpackage Home Model
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
class HomeModel extends Initialize {
    
    /**
	 * Method getPerson($idPerson)
	 *
     * Get all DB's informations about the person selected in parameter, section in homepage
	 */  
    public function getPerson($idPerson){
        $req = 'SELECT * FROM person
                WHERE person_id = :idPerson';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idPerson", $idPerson, PDO::PARAM_INT);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }

    /**
	 * Method getResume()
	 *
     * Get all DB's informations about what can I do, section in homepage
	 */  
    public function getResume(){
        $req = 'SELECT resume_title as title, resume_description as description, resume_image as image, resume_alt as alt FROM resume';
        $stmt = $this->getDB()->prepare($req);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }

    /**
	 * Method getResume()
	 *
     * Get all DB's informations about news projects in homapage
	 */  
    public function getNewsProjects(){
        $req = 'SELECT project.project_id as id, image_url as image, image_alt as alt 
                FROM project
                INNER JOIN image ON image.project_id = project.project_id
                INNER JOIN person__project as pp ON pp.project_id = project.project_id
                WHERE image_thumb = 0 AND pp.person_id = 1
                ORDER BY project.project_id DESC LIMIT 3';
        $stmt = $this->getDB()->prepare($req);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }
}