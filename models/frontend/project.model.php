<?php
require_once "./models/initialize.php";

class ProjectModel extends Initialize {
    public function getProjectInfos($idProject){
        $req = 'SELECT * FROM project
                WHERE project_id = :idProject';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idProject", $idProject, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }

    public function getProjectImages($idProject){
        $req = 'SELECT image_url as url, image_alt as alt FROM image
                WHERE project_id = :idProject AND image_thumb = 1';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idProject", $idProject, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }
    public function getProjectImagesActive($idProject){
        $req = 'SELECT * FROM image 
                WHERE project_id = :idProject AND image_thumb = 1 LIMIT 1';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idProject", $idProject, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }   

    public function getProjectTag($idProject){
        $req = 'SELECT tag_name as tag FROM tag
                INNER JOIN project__tag as pt ON tag.tag_id = pt.tag_id
                WHERE project_id = :idProject';
        $stmt = $this->getDB()->prepare($req);
        $stmt->bindValue(":idProject", $idProject, PDO::PARAM_STR);
        $stmt->execute();
        $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $datas;
    }
    
}