<?php
require_once "config/config.php";

/**
 * Class Initialize | file initialize.php
 * 
 * In this class, we have methods for :
 * 
 * - Connect DB
 *
 * Is required:
 * 
 * - require "./config/config.php";
 *
 * @package Portfolio
 * @subpackage Initialize
 * @author Laury Devraigne
 * @copyright  2021-2071 Laury Devraigne
 * @version v1.0
 */
abstract class Initialize {
    /**
     * private attribute bdd 
     * 
     * Is used to store all datas needed for create DB connection
     * 
     * @var object
     * 
     */
    private static $bdd;

    /**
	 * Method setDB()
	 *
     * setting connection DB
	 */ 
    private static function setDB(){
        try{
            self::$bdd = new PDO("mysql:host=".HOST_NAME.";dbname=".DATABASE_NAME.";charset=utf8",USER_NAME,PASSWORD);
            self::$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        }
        catch(Exception $e){
            $errorMessage = "Une erreur est survenue";
            echo $errorMessage;
            die();
        }
    }
  
    /**
	 * Method getDB()
	 *
     * get connection DB
	 */ 
    protected function getDB(){
        if(self::$bdd === null){
            self::setDB();
        }
        return self::$bdd;
    }
}