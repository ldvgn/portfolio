/******************/
/* SERVICE WORKER */
/******************/
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('./service-worker.js')
    });
}

/******************/
/* SERVICE WORKER */
/******************/
// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function() {
//         // navigator.serviceWorker.register("./service-worker.js").then(registration => {
//         //     console.log("success!");
//         //     if (registration.installing) {
//         //         registration.installing.postMessage("Howdy from your installing page.");
//         //     }
//         // }, err => {
//         //     console.error("Installing the worker failed!", err);
//         // });

//         navigator.serviceWorker.register("./service-worker.js")
//         .then(registration => {
//                 console.log("success!")})
//         .catch(() => {
//             console.error("Install failed due to the path restriction violation.");
//         });
//     });
// }

// if ('serviceWorker' in navigator) {
//     // declaring scope manually
//     navigator.serviceWorker.register('./service-worker.js', { scope: '/' }).then(
//       (registration) => {
//         console.log('Service worker registration succeeded:', registration)
//       },
//       /*catch*/ (error) => {
//         console.log('Service worker registration failed:', error)
//       }
//     )
//   } else {
//     console.log('Service workers are not supported.')
//   }

/*************************************/
/* PROMPT TO INSTALL APP ON DESKTOP: */
/*************************************/
let deferredPrompt;
window.addEventListener('beforeinstallprompt', event => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    event.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = event;

    // Attach the install prompt to a user gesture
    document.querySelector('#installBtn').addEventListener('click', event => {

        // Show the prompt
        deferredPrompt.prompt();

        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                $("#installBanner").hide();
            }
            deferredPrompt = null;
        });
    });

    // Update UI notify the user they can add to home screen
    document.querySelector('#installBanner').style.display = 'flex';

    // Hide banner and Store the situation into a var session.
    $("#closeBtn").click(function(){
        $("#installBanner").hide();
        sessionStorage.setItem('InstallApp', '1');
    });

    var data = sessionStorage.getItem('InstallApp');
    if (data === '1'){
        $("#installBanner").hide();
    }
});

/**********/
/* FILTER */
/**********/
var aItems = document.querySelectorAll('.projects-item--tag-link');
var sAlert = document.getElementById("projects-alert");

aItems.forEach(function(item){
    item.addEventListener('click', function(){
        var tag = item.getAttribute("name");
        var aItemsAll = document.querySelectorAll('.projects-list');
        
        if (tag !== "all"){
            var aItemsFilter = document.querySelectorAll('.'+ tag);

            if (aItemsFilter.length == 0){
                aItemsAll.forEach(function(project){
                    project.style.display = "none";
                });
                sAlert.style.display = "block";
            } else{
                sAlert.style.display = "none";
                aItemsAll.forEach(function(project){
                    project.style.display = "none";
                });
        
                aItemsFilter.forEach(function(project){
                    project.style.display = "flex";
                });
            }
        } else{
            sAlert.style.display = "none";
            aItemsAll.forEach(function(project){
                project.style.display = "flex";
            });
        }
    });
});
